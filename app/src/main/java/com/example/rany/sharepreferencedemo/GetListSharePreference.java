package com.example.rany.sharepreferencedemo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.rany.sharepreferencedemo.constant.AppConstant;
import com.example.rany.sharepreferencedemo.model.Article;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetListSharePreference extends AppCompatActivity {

    @BindView(R.id.article_result)
    ListView result;

    private SharedPreferences preferences;
    private ArrayList<String> articleList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_list_share_preference);
        ButterKnife.bind(this);

        preferences = getSharedPreferences(AppConstant.ARTICLE_PREF, MODE_PRIVATE);
        String articleJson = preferences.getString(AppConstant.ARTICLE_OBJ, "n/a");

        String articleLists = preferences.getString(AppConstant.ARTICLE_LIST, "n/a");

        if(articleJson != null){
            Type type = new TypeToken<List<Article>>(){}.getType();
            List<Article> articles = new Gson().fromJson(articleLists, type);

            articleList = new ArrayList<>();
            for(Article item : articles){
                articleList.add(item.getTitle());
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, articleList);
            result.setAdapter(arrayAdapter);

        }


    }
}
