package com.example.rany.sharepreferencedemo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.rany.sharepreferencedemo.constant.AppConstant;
import com.example.rany.sharepreferencedemo.model.Article;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetPreferenceDemo extends AppCompatActivity {

    @BindView(R.id.tvGetName)
    TextView getName;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_preference_demo);
        ButterKnife.bind(this);

        preferences = getSharedPreferences(AppConstant.PREF_NAME, MODE_PRIVATE);
        String result = preferences.getString(AppConstant.PREF_NAME, "n/a");
        String article = preferences.getString(AppConstant.SHARE_OBJ, "n/a");

        Article article1 = new Gson().fromJson(article, Article.class);

        getName.setText(article1.getTitle());

    }
}
