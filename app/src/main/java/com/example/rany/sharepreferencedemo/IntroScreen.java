package com.example.rany.sharepreferencedemo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class IntroScreen extends AppIntro {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance(
                "Title 1",
                "Description 1",
                R.drawable.panda,
                getApplicationContext().getColor(R.color.colorAccent)));
        addSlide(AppIntroFragment.newInstance(
                "Title 2",
                "Description 2",
                R.drawable.panda,
                getApplicationContext().getColor(R.color.colorPrimary)));

        addSlide(AppIntroFragment.newInstance(
                "Title 3",
                "Description 3",
                R.drawable.panda,
                getApplicationContext().getColor(R.color.colorPrimaryDark)));


        showSkipButton(true);
        setProgressButtonEnabled(true);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        MyPreference.setPreference(this, 2);
        startActivity(new Intent(this, LoginScreen.class));
        finish();
    }
}
