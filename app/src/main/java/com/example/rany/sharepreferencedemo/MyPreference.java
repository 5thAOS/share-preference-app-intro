package com.example.rany.sharepreferencedemo;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.rany.sharepreferencedemo.constant.AppConstant;

public class MyPreference {

    public static void setPreference(Context context, int status){

        SharedPreferences sharedPreferences =
                context.getSharedPreferences(AppConstant.STATUS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(AppConstant.STATUS_VALUE, status);
        editor.apply();
    }

    public static int getPreferences(Context context){
        SharedPreferences sharedPreferences =
                context.getSharedPreferences(AppConstant.STATUS, Context.MODE_PRIVATE);
        int a =  sharedPreferences.getInt(AppConstant.STATUS_VALUE, 1);
        return a;
    }

}
