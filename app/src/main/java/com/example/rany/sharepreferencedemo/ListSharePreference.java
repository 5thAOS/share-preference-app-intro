package com.example.rany.sharepreferencedemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.example.rany.sharepreferencedemo.constant.AppConstant;
import com.example.rany.sharepreferencedemo.model.Article;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListSharePreference extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private List<Article> articles;
    private int id = 1;

    @BindView(R.id.article_title)
    EditText title;

    @BindView(R.id.article_desc)
    EditText desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_share_preference);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(AppConstant.ARTICLE_PREF, MODE_PRIVATE);

        String articleJson = sharedPreferences.getString(AppConstant.ARTICLE_LIST, null);
        if(articleJson == null){
            articles = new ArrayList<>();
        }
        else{
            Type type = new TypeToken<List<Article>>(){}.getType();
            articles = new Gson().fromJson(articleJson, type);
        }
    }

    @OnClick(R.id.article_share)
    public void shareListPreference(){

        Article article = new Article(id++, title.getText().toString(), desc.getText().toString());
        articles.add(article);

        editor = sharedPreferences.edit();
        editor.putString(AppConstant.ARTICLE_OBJ, new Gson().toJson(article));
        editor.putString(AppConstant.ARTICLE_LIST, new Gson().toJson(articles));
        editor.apply();

        startActivity(new Intent(this, GetListSharePreference.class));
    }
    @OnClick(R.id.article_check)
    public void checkSharePreference(){
        startActivity(new Intent(this, GetListSharePreference.class));
    }
}
