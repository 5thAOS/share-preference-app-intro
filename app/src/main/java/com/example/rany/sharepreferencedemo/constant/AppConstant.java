package com.example.rany.sharepreferencedemo.constant;

public class AppConstant {

    // Primitive and single object constant
    public static final String PREF_NAME = "prefe_name";
    public static final String SHARE_NAME = "share_name";
    public static final String SHARE_OBJ = "share_obj";

    // List object
    public static final String ARTICLE_PREF = "article_prefe";
    public static final String ARTICLE_OBJ = "article_obj";
    public static final String ARTICLE_LIST = "article_list";


    public static final String Log = "oooo";
    public static final String STATUS = "0";
    public static final String STATUS_VALUE = "value";
}
