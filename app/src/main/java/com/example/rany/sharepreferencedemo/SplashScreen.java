package com.example.rany.sharepreferencedemo;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final int status = MyPreference.getPreferences(this);

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        switch (status){
                            case 1:
                                startActivity(new Intent(SplashScreen.this, IntroScreen.class));
                                break;
                            case 2:
                                startActivity(new Intent(SplashScreen.this, LoginScreen.class));
                                break;
                            case 3:
                                startActivity(new Intent(SplashScreen.this, HomeScreen.class));
                                break;
                        }
                    }
                }
                , 5000);

    }
}
