package com.example.rany.sharepreferencedemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.example.rany.sharepreferencedemo.constant.AppConstant;
import com.example.rany.sharepreferencedemo.model.Article;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SharePreferenceDemo extends AppCompatActivity {

    @BindView(R.id.tvName)
    EditText name;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnGetShare)
    public void getShareValue(){

        Article article = new Article(1, name.getText().toString(), "E learning platform in Cambodia");

        String articles = new Gson().toJson(article);

        preferences = getSharedPreferences(AppConstant.PREF_NAME, MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(AppConstant.SHARE_NAME, name.getText().toString());
        editor.putString(AppConstant.SHARE_OBJ, articles);
        editor.apply();

        startActivity(new Intent(this, GetPreferenceDemo.class));
    }

}
